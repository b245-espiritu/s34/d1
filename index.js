
// Load the express module into our application
// and save it in a variable called express.


const express = require('express');


//localhost port number
const port = 4000;


// app is our server
// create an application that uses and stores it as app.
const app = express();


// middleware
// express.json() - is a method which allow us to handle the
//streaming of data and automatically parse the incoming json from our request.
app.use(express.json());



//mockdata
let users = [
    {
        username:"TStart3000",
        email: "starkinustries@mail.com"
    },
    {
        username: "ThorThunder",
        email: "loveandthunder@mail.com",
        password: "iLoveStormBreaker"
    }
]


// express has methods to use as route corresponding to HTTP methods


/**
 * Syntax:
 * 
 *  app.method(<endpoint>, function for request and response)
 */



// [HTTP method GET]

    app.get("/", (request, response) =>{

        // response.status = (201) is equivalent to  writeHead
        // response.send = write with end()
        response.send("Hellow from express!")
    })

   
    // mini activity:
    /**
        create a 'get' route in expressjs which
        will be able to send a message in the client:

        endpoint: /greeting
        message: 'hellow from Batch245-surename'
        status code :201
     */

        app.get("/greeting", (request, response) =>{

            response.status(200).send("hellow from Batch245-surename")
    
        })
    

        app.get("/users", (request, response)=>{
            response.send(users)
        })
    

        // [HTTP method for POST]
        app.post( "/users", (request, response)=>{
            let input = request.body;

            let newUser = {
                username: input.username,
                email: input.email,
                password: input.password
            }

            users.push(newUser)

            response.send(` The ${newUser.username} is now register in our website with ${newUser.email}`)

            response.send(input);
        })

        
        // [HTTP method for DELETE]
        app.delete("/users", (request, response)=>{
           let deletedUser = users.pop();
           response.send(deletedUser);
        })


        // [HTTP method for PUT]
        // PUT and PATCH is different in express.js

        app.put("/users/:index", (request, response) =>{

            let indexToBeUpdated = Number(request.params.index); //used Number to convert to Number
                // indexToBeUpdated = parseInt(indexToBeUpdated);
                
                console.log(typeof(indexToBeUpdated));

            if(indexToBeUpdated < users.length){
                users[indexToBeUpdated].password = request.body.password;
                response.send(users[indexToBeUpdated]);
            }
            else{
                response.status(404).send('page not found')
            }
             
            
           
            
        })
        
        // listen method
        app.listen(port, ()=> console.log('Server is running at port 4000'))